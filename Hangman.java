import java.util.Scanner;
public class Hangman{

	public static int isLetterInWord(String word, char c) {
	//makes a for loop to check every letter in the word the user is guessing
	for (int i =0; i<4 ; i++){
		//checks if the letter is guessed correctly to return i or minus one
		if (word.charAt(i)==c){
			return i;
		}
			
	}
	   return -1;
}
	public static char toUpperCase(char c) {
		//turns character to uppercase
		c = Character.toUpperCase(c);
		return c;
	}
	public static void printWork(String word, boolean[] letters) {
    //initializes an empty string to build the result
    String newWord = "";

    //iterate through each character in the word and corresponding boolean in the array
    for (int i = 0; i < word.length() && i < letters.length; i++) {
        //checks if the letter should be revealed (true in the boolean array)
        if (letters[i]) {
            //appends the actual letter to the result string
            newWord += word.charAt(i);
        } else {
            //appends an underscore for hidden letters
            newWord += "_";
        }
    }

   
    System.out.println("Your result is " + newWord);
}



	public static void runGame(String word) {
		// Initializes variables to track guessed letters and guesses count
		boolean letter0 = false;
		boolean letter1 = false;
		boolean letter2 = false;
		boolean letter3 = false;
		int numberOfGuess = 0;

		// Main game loop: continue until 6 guesses or all letters are guessed
		while (numberOfGuess < 6 && !(letter0 && letter1 && letter2 && letter3)) {

        // Gets user input
        Scanner reader = new Scanner(System.in);
        System.out.println("Guess a letter");
        char guess = reader.nextLine().charAt(0);
        guess = toUpperCase(guess); // Convert input to uppercase

        // Checks if the guessed letter is in the word and update corresponding variables
        if (isLetterInWord(word, guess) == 0) {
            letter0 = true;
        }
        if (isLetterInWord(word, guess) == 1) {
            letter1 = true;
        }
        if (isLetterInWord(word, guess) == 2) {
            letter2 = true;
        }
        if (isLetterInWord(word, guess) == 3) {
            letter3 = true;
        }
        if (isLetterInWord(word, guess) == -1) {
            numberOfGuess++;
        }

        // Creates an array to represent guessed letters and print the current result
        boolean[] letters = {letter0, letter1, letter2, letter3};
        printWork(word, letters);
    }

    // Displays game outcome based on the result
    if (numberOfGuess == 6) {
        System.out.println("Oops! Better luck next time :)");
    }
    if (letter0 && letter1 && letter2 && letter3) {
        System.out.println("Congrats! You got it :)");
    }
}

	//public static void main(String[] args){
		//Scanner reader = new Scanner (System.in);
		//Get user input
		//System.out.println("Enter a 4-letter word:");
		//String word = reader.next();
		//Convert to upper case
		//word = word.toUpperCase();
		
		//Start hangman game
	    //runGame(word);	
	//}
}