import java.util.*;
//This asks the user to input an int 1 or 2
//It checks which Int they have chosen and runs whichever game based off that
//If the user has not put an integer 1 or 2, the program exits and lets them know why
public class GameLauncher{
	
	public static void main(String[] args){
		System.out.println("Hello! Enter '1' to play Hangman and '2' to play Wordle");
		Scanner reader = new Scanner(System.in);
		int whichGame = reader.nextInt();
		
		if(whichGame == 1){
			System.out.println("Enter a 4-letter word:");
			String word = reader.next();
			word = word.toUpperCase();
			Hangman.runGame(word);
			
		} else if(whichGame == 2){
			String answer = Wordle.generateWord();
			Wordle.runGame(answer);
			
		} else { 
			System.out.println("This program will exit, since you did not enter '1' or '2'.");
		}
	}
}
	